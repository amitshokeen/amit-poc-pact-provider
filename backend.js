const express = require("express")
const cors = require("cors")

const server = express()
server.use(cors())
server.use((req, res, next) => {
  res.header("Content-Type", "application/json; charset=utf-8")
  next()
})

server.get("/v1.0/settings/get-user-languages", (req, res) => {
  res.json([
    {
        "languageID": 1,
        "LanguageName": "English",
        "Locale": "en-AU"
    },
    {
        "LanguageID": 8,
        "LanguageName": "한국어",
        "Locale": "ko"
    },
    {
        "LanguageID": 45,
        "LanguageName": "简体中文",
        "Locale": "zh"
    },
    {
        "LanguageID": 46,
        "LanguageName": "繁體中文",
        "Locale": "zh-TW"
    }
  ]) 
})

module.exports = {
  server
}